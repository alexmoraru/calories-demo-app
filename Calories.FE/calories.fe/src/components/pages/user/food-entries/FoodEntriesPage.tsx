import React, { useEffect, useState } from "react";
import { IColumn, IconButton, IDetailsGroupRenderProps, IGroup, IGroupDividerProps, PrimaryButton, SelectionMode, ShimmeredDetailsList } from "@fluentui/react";
import clsx from "clsx";

import FoodEntriesService from "../../../../common/api-services/FoodEntriesService";
import UsersService from "../../../../common/api-services/UsersService";
import FoodEntryListViewOut from "../../../../common/models/api-models/FoodEntryListViewOut";
import AddFoodEntryPanel from "./add-panel/AddFoodEntryPanel";
import ArrayHelper from "../../../../common/utils/ArrayHelper";
import { useSnackbar } from "../../../common/snackbar-provider/SnackbarProvider";
import { NotificationType } from "../../../../common/models/SnackbarNotification";

import styles from "./FoodEntries.module.scss";

const FoodEntriesPages: React.FC = () => {
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [entries, setEntries] = useState<FoodEntryListViewOut[]>([]);
    const [entryGroups, setEntryGroups] = useState<IGroup[]>([]);
    const [isPanelOpen, togglePanel] = useState<boolean>(false);
    const [caloriesLimit, setCaloriesLimit] = useState<number>(0);

    const { enqueueSnackbar } = useSnackbar();

    const columns: IColumn[] = [
        {
            key: "name",
            name: "Name",
            fieldName: "name",
            minWidth: 150,
            maxWidth: 250
        },
        {
            key: "calories",
            name: "Calories",
            fieldName: "calories",
            minWidth: 150,
            maxWidth: 250
        },
        {
            key: "meal",
            name: "Meal",
            fieldName: "meal",
            minWidth: 150,
            maxWidth: 250
        }
    ];

    useEffect(() => {
        setIsLoading(true);

        const promises: Promise<any>[] = [
            UsersService.getCaloriesLimit().then(
                data => {
                    setCaloriesLimit(data);
                },
                (errors) => {
                    enqueueSnackbar({
                        text: errors,
                        type: NotificationType.error
                    });
                }),
            FoodEntriesService.get().then(
                data => {
                    setEntries(data);
                    computeGroups(data);
                },
                (errors) => {
                    enqueueSnackbar({
                        text: errors,
                        type: NotificationType.error
                    });
                })
        ];

        Promise.all(promises).then(
            () => {
                setIsLoading(false);
            },
            (errors) => {
                enqueueSnackbar({
                    text: errors,
                    type: NotificationType.error
                });
                setIsLoading(false);
            }
        )
        // eslint-disable-next-line
    }, []);

    const computeGroups = (data: FoodEntryListViewOut[]) => {
        const groups: IGroup[] = [];

        let currentGroup: IGroup | null = null;
        data.forEach((item, index) => {
            if (!currentGroup || item.created.toString() !== currentGroup.key) {
                if (currentGroup) {
                    groups.push(currentGroup);
                }

                currentGroup = {
                    key: item.created.toString(),
                    name: new Date(item.created).toLocaleString("en-GB", {
                        day: "numeric",
                        month: "short",
                        year: "numeric"
                    }),
                    startIndex: index,
                    count: 1,
                    level: 0,
                    data: {
                        calories: item.calories
                    }
                };
            } else {
                currentGroup.count++;
                currentGroup.data.calories += item.calories;
            }
        });
        if (!!currentGroup) {
            groups.push(currentGroup);
        }

        setEntryGroups(groups);
    }

    const onToggleCollapse = (props: IGroupDividerProps) => {
        return () => {
            props!.onToggleCollapse!(props!.group!);
        };
    }

    const onRenderGroupHeader: IDetailsGroupRenderProps["onRenderHeader"] = (props) => {
        if (!props) {
            return null;
        }

        return (
            <div className={styles.groupHeaderContainer}>
                <IconButton
                    iconProps={{ iconName: props.group!.isCollapsed ? "ChevronDown" : "ChevronUp" }}
                    onClick={onToggleCollapse(props)}
                />
                <div className={styles.groupName}>
                    <span>{`${props.group!.name} (${props.group!.count})`}</span>
                    <span className={clsx(props.group!.data.calories > caloriesLimit && styles.warning)}>Calories: {props.group!.data.calories}</span>
                </div>
            </div>
        );
    }

    const onSave = (data: FoodEntryListViewOut) => {
        let newEntries = [...entries, data];
        newEntries = ArrayHelper.sort(newEntries, "created", true);
        setEntries(newEntries);

        computeGroups(newEntries);

        togglePanel(false);
    }

    return (
        <div>
            <PrimaryButton
                text="Add new entry"
                onClick={() => { togglePanel(true); }}
                className={styles.addButton}
            />
            <ShimmeredDetailsList
                columns={columns}
                items={entries}
                groups={entryGroups}
                groupProps={{
                    onRenderHeader: onRenderGroupHeader
                }}
                enableShimmer={isLoading}
                selectionMode={SelectionMode.none}
            />
            <AddFoodEntryPanel
                visible={isPanelOpen}
                save={onSave}
                cancel={() => { togglePanel(false); }}
            />
        </div>
    )
};

export default FoodEntriesPages;