import React, { useEffect, useState } from "react";
import { DatePicker, DefaultButton, Dropdown, IDropdownOption, MessageBar, MessageBarType, Panel, Position, PrimaryButton, ProgressIndicator, SpinButton, TextField } from "@fluentui/react";

import FoodEntryListViewOut from "../../../../../common/models/api-models/FoodEntryListViewOut";
import MealsService from "../../../../../common/api-services/MealsService";
import useOfficeForms, { FieldTypes } from "../../../../common/hooks/useOfficeForms";
import FoodEntriesService from "../../../../../common/api-services/FoodEntriesService";
import DateHelper from "../../../../../common/utils/DateHelper";

import styles from "./AddFoodEntryPanel.module.scss";

const AddFoodEntryPanel: React.FC<{
    visible: boolean,
    save: (data: FoodEntryListViewOut) => void,
    cancel: () => void
}> = ({ visible, save, cancel }) => {

    const [saveInProgress, setSaveInProgress] = useState<boolean>(false);
    const [apiErrors, setApiErrors] = useState<string[] | undefined>();
    const [meals, setMeals] = useState<IDropdownOption[]>([]);
    const [calories, setCalories] = useState<number>(1);

    const [errors, setErrors] = useState<any>({});

    const { register, validate } = useOfficeForms();

    useEffect(() => {
        if (visible) {
            setSaveInProgress(false);
            setApiErrors(undefined);
            setErrors({});
            setCalories(1);

            if (!meals || !meals.length) {
                MealsService.get().then(data => {
                    setMeals(data.map(x => ({ key: x.id, text: x.name })));
                });
            }
        }
        // eslint-disable-next-line
    }, [visible]);

    const saveEntry = () => {
        const { values, errors, isFormValid } = validate();

        if (!isFormValid) {
            setErrors(errors);

            return;
        }

        setSaveInProgress(true);
        setErrors({});

        const data = { ...values, created: new Date(values.created), calories };

        FoodEntriesService.add(data).then(
            id => {
                save({
                    id,
                    ...data,
                    created: `${values.created}T00:00:00`,
                    meal: meals.filter(x => x.key === data.mealId)[0].text
                });
                setSaveInProgress(false);
            },
            (apiErrors) => {
                setSaveInProgress(false);
                setApiErrors(apiErrors);
            });
    }

    const onRenderFooterContent = () => {
        return (
            <>
                {
                    apiErrors &&
                    <MessageBar
                        messageBarType={MessageBarType.error}
                        isMultiline
                        truncated={!!apiErrors.length}
                        styles={{ root: { marginBottom: 5 } }}
                    >
                        {apiErrors}
                    </MessageBar>
                }
                {
                    saveInProgress ?
                        <ProgressIndicator
                            description="Saving food entry..."
                            barHeight={5}
                            styles={{ itemDescription: { marginRight: "15px" } }}
                        />
                        :
                        <div className={styles.buttons}>
                            <PrimaryButton onClick={saveEntry}>Save</PrimaryButton>
                            <DefaultButton onClick={cancel}>Cancel</DefaultButton>
                        </div>
                }
            </>
        )
    }

    return (
        <Panel
            headerText="Add new food entry"
            isOpen={visible}
            onDismiss={cancel}
            onRenderFooterContent={onRenderFooterContent}
            isFooterAtBottom
        >
            <TextField
                id="name"
                label="Name"
                required
                maxLength={250}
                componentRef={register}
                errorMessage={errors.name}
                disabled={saveInProgress}
            />
            <SpinButton
                id="calories"
                label="Calories"
                labelPosition={Position.top}
                min={1}
                step={1}
                value={String(calories)}
                onChange={(_, value) => { console.log(value); setCalories(value ? +value : 1); }}
                incrementButtonAriaLabel="Increase value by 1"
                decrementButtonAriaLabel="Decrease value by 1"
                disabled={saveInProgress}
            />
            <Dropdown
                id="mealId"
                placeholder="Select a meal"
                label="Meal"
                options={meals}
                required
                componentRef={input => { register(input, FieldTypes.Dropdown); }}
                errorMessage={errors.mealId}
                disabled={saveInProgress}
            />
            <DatePicker
                id="created"
                label="Date"
                isRequired
                formatDate={DateHelper.GetSimpleDateString}
                textField={{
                    disabled: saveInProgress,
                    required: true,
                    errorMessage: errors.created,
                    componentRef: (input) => register(input, FieldTypes.DatePicker)
                }}
                disabled={saveInProgress}
            />
        </Panel>
    )
};

export default AddFoodEntryPanel;