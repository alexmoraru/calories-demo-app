import React, { useState } from "react";
import { PrimaryButton, ProgressIndicator, TextField } from "@fluentui/react";

import useOfficeForms from "../../common/hooks/useOfficeForms";
import UsersService from "../../../common/api-services/UsersService";
import { useSnackbar } from "../../common/snackbar-provider/SnackbarProvider";
import { NotificationType } from "../../../common/models/SnackbarNotification";

import styles from "./InviteFriend.module.scss";

const InviteFriendPage: React.FC = () => {
    const { register, validate } = useOfficeForms();
    const [errors, setErrors] = useState<any>({});
    const [saveInProgress, setSaveInProgress] = useState<boolean>(false);
    const [inviteToken, setInviteToken] = useState<string>("");
    const [name, setName] = useState<string>("");
    const [email, setEmail] = useState<string>("");
    const { enqueueSnackbar } = useSnackbar();

    const save = () => {
        setInviteToken("");

        const { values, errors, isFormValid } = validate();

        if (!isFormValid) {
            setErrors(errors);

            return;
        }

        setErrors({});
        setSaveInProgress(true);

        UsersService.invite(values).then(
            result => {
                setInviteToken(result.token);
                setName("");
                setEmail("");
                setSaveInProgress(false);
            },
            errors => {
                enqueueSnackbar({
                    text: errors,
                    type: NotificationType.error
                });
                setSaveInProgress(false);
            }
        )
    }

    return (
        <div>
            <h2>Invite a friend</h2>
            <TextField
                id="name"
                label="Name"
                required
                maxLength={150}
                value={name}
                onChange={(_, value) => setName(value || "")}
                componentRef={register}
                errorMessage={errors.name}
            />
            <TextField
                id="email"
                label="Email"
                required
                maxLength={100}
                value={email}
                onChange={(_, value) => setEmail(value || "")}
                componentRef={register}
                errorMessage={errors.email}
            />
            <div className={styles.inviteAction}>
                {
                    saveInProgress ?
                        <ProgressIndicator label="Inviting in progress..." />
                        :
                        <PrimaryButton text="Invite" onClick={save} />
                }
            </div>
            {
                inviteToken &&
                <div className={styles.tokenDisplay}>
                    The token resulted from the invitation is: {inviteToken}
                </div>
            }
        </div>
    )
};

export default InviteFriendPage;