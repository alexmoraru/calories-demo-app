import React, { useState } from "react";
import { PrimaryButton, ProgressIndicator, TextField } from "@fluentui/react";
import jwtDecode from "jwt-decode";

import useOfficeForms, { FieldTypes } from "../../common/hooks/useOfficeForms";
import UsersService from "../../../common/api-services/UsersService";
import { UserType } from "../../../common/constants/UserType";
import TokenService from "../../../common/services/TokenService";
import { useSnackbar } from "../../common/snackbar-provider/SnackbarProvider";
import { NotificationType } from "../../../common/models/SnackbarNotification";

import styles from "./LoginPage.module.scss";

const LoginPage: React.FC<{ onLoggedIn: (type: UserType) => void }> = ({ onLoggedIn }) => {

    const { register, validate } = useOfficeForms();
    const [errors, setErrors] = useState<any>({});
    const { enqueueSnackbar } = useSnackbar();
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const login = () => {
        const { isFormValid, values, errors } = validate();
        if (!isFormValid) {
            setErrors(errors);

            return;
        }

        setErrors({});
        setIsLoading(true);

        UsersService.getToken(values.email).then(
            token => {
                TokenService.setToken(token);
                const decodedToken: any = jwtDecode(token);
                const role: number = decodedToken["role"];
                onLoggedIn(role as UserType);
                setIsLoading(false);
            },
            (error) => {
                enqueueSnackbar({
                    text: error,
                    type: NotificationType.error
                });
                setIsLoading(false);
            });
    }

    return (
        <div className={styles.pageContainer}>
            <div className={styles.loginForm}>
                <div className={styles.loginTitle}>Login</div>
                <TextField
                    id="email"
                    label="Email"
                    required
                    componentRef={(input) => register(input, FieldTypes.Email)}
                    errorMessage={errors.email}
                    className={styles.emailInput}
                />
                {
                    isLoading ?
                        <ProgressIndicator label="Logging in..." />
                        :
                        <PrimaryButton text="Login" onClick={login} />
                }
            </div>
        </div>
    );
};

export default LoginPage;