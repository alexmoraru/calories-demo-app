import { Spinner, SpinnerSize } from "@fluentui/react";
import React, { useEffect, useState } from "react";
import FoodEntriesService from "../../../../../common/api-services/FoodEntriesService";
import FoodEntriesStatisticsPreviousWeeksOut from "../../../../../common/models/api-models/FoodEntriesStatisticsPreviousWeeksOut";
import { NotificationType } from "../../../../../common/models/SnackbarNotification";
import { useSnackbar } from "../../../../common/snackbar-provider/SnackbarProvider";

import styles from "./WeeksStatistics.module.scss";

const WeeksStatistics: React.FC = () => {
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [data, setData] = useState<FoodEntriesStatisticsPreviousWeeksOut>({
        lastWeek: 0,
        previousToLastWeek: 0
    });
    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        setIsLoading(true);
        FoodEntriesService.getWeeksStatistics().then(
            statistics => {
                setData(statistics);
                setIsLoading(false);
            },
            (errors) => {
                enqueueSnackbar({
                    text: errors,
                    type: NotificationType.error
                });
                setIsLoading(false);
            }
        )
        // eslint-disable-next-line
    }, []);

    return (
        <div className={styles.container}>
            <div className={styles.card}>
                {
                    isLoading ?
                        <Spinner size={SpinnerSize.large} />
                        :
                        <h1>{data.lastWeek}</h1>
                }
                <h4>Added entries in the last 7 days</h4>
            </div>
            <div className={styles.card}>
            {
                    isLoading ?
                        <Spinner size={SpinnerSize.large} />
                        :
                        <h1>{data.previousToLastWeek}</h1>
                }
                <h4>Added entries in the previous to last week</h4>
            </div>
        </div>
    );
};

export default WeeksStatistics;