import React, { useEffect, useState } from "react";
import { Spinner, SpinnerSize } from "@fluentui/react";
import Plot from 'react-plotly.js';

import FoodEntriesService from "../../../../../common/api-services/FoodEntriesService";
import FoodEntriesStatisticsAverageOut from "../../../../../common/models/api-models/FoodEntriesStatisticsAverageOut";
import { useSnackbar } from "../../../../common/snackbar-provider/SnackbarProvider";
import { NotificationType } from "../../../../../common/models/SnackbarNotification";

import styles from "./AverageStatistics.module.scss";

const AverageStatistics: React.FC = () => {

    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [data, setData] = useState<FoodEntriesStatisticsAverageOut[]>([]);
    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        setIsLoading(true);
        FoodEntriesService.getAverageStatistics().then(
            statistics => {
                setData(statistics);
                setIsLoading(false);
            },
            (errors) => {
                enqueueSnackbar({
                    text: errors,
                    type: NotificationType.error
                });
                setIsLoading(false);
            }
        )
        // eslint-disable-next-line
    }, []);

    return (
        <div className={styles.container}>
            {
                isLoading ?
                    <Spinner size={SpinnerSize.large} />
                    :
                    <Plot
                        data={[
                            {
                                type: "bar",
                                orientation: "h",
                                x: data.map(x => x.calories),
                                y: data.map(x => x.userName)
                            }
                        ]}
                        layout={{ width: window.innerWidth / 2, height: window.innerHeight / 2, title: 'Average number of calories in the last 7 days' }}
                    />
            }
        </div>
    );
};

export default AverageStatistics;