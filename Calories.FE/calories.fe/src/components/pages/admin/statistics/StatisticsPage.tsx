import React from "react";

import WeeksStatistics from "./weeks/WeeksStatistics";
import AverageStatistics from "./average/AverageStatistics";

import styles from "./StatisticsPage.module.scss";

const StatisticsPage: React.FC = () => {
    return (
        <div className={styles.container}>
            <WeeksStatistics />
            <AverageStatistics />
        </div>
    )
};

export default StatisticsPage;