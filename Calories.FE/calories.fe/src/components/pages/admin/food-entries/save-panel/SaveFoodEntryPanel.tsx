import React, { useEffect, useState } from "react";
import { DatePicker, DefaultButton, Dropdown, IDropdownOption, MessageBar, MessageBarType, Panel, Position, PrimaryButton, ProgressIndicator, SpinButton, TextField } from "@fluentui/react";

import FoodEntryUserListViewOut from "../../../../../common/models/api-models/FoodEntryUserListViewOut";
import MealsService from "../../../../../common/api-services/MealsService";
import useOfficeForms, { FieldTypes } from "../../../../common/hooks/useOfficeForms";
import FoodEntriesService from "../../../../../common/api-services/FoodEntriesService";
import DateHelper from "../../../../../common/utils/DateHelper";
import UsersService from "../../../../../common/api-services/UsersService";

import styles from "./SaveFoodEntryPanel.module.scss";

const SaveFoodEntryPanel: React.FC<{
    visible: boolean,
    data: FoodEntryUserListViewOut | undefined,
    save: (data: FoodEntryUserListViewOut) => void,
    cancel: () => void
}> = ({ visible, data, save, cancel }) => {
    const [saveInProgress, setSaveInProgress] = useState<boolean>(false);
    const [apiErrors, setApiErrors] = useState<string[] | undefined>();
    const [meals, setMeals] = useState<IDropdownOption[]>([]);
    const [users, setUsers] = useState<IDropdownOption[]>([]);
    const [calories, setCalories] = useState<number>(1);
    const [createdDate, setCreatedDate] = useState<Date>(new Date());

    const [errors, setErrors] = useState<any>({});

    const { register, validate } = useOfficeForms();

    useEffect(() => {
        if (visible) {
            setSaveInProgress(false);
            setApiErrors(undefined);
            setErrors({});
            setCalories(data?.calories || 1);
            setCreatedDate(data ? new Date(data?.created) : new Date());

            if (!meals || !meals.length) {
                MealsService.get().then(data => {
                    setMeals(data.map(x => ({ key: x.id, text: x.name })));
                });
            }

            if (!users || !users.length) {
                UsersService.getAll().then(data => {
                    setUsers(data.map(x => ({ key: x.id, text: x.name })));
                });
            }
        }
        // eslint-disable-next-line
    }, [visible]);

    const saveEntry = () => {
        const { values, errors, isFormValid } = validate();

        if (!isFormValid) {
            setErrors(errors);

            return;
        }

        setSaveInProgress(true);
        setErrors({});

        const saveData = { ...values, created: new Date(values.created), calories };

        let savePromise;
        if (data) {
            savePromise = FoodEntriesService.update(data.id, saveData);
        } else 
        {
            savePromise = FoodEntriesService.addForUser(saveData);
        }

        savePromise.then(
            id => {
                save({
                    id: data?.id || id,
                    ...saveData,
                    created: `${values.created}T00:00:00`,
                    meal: meals.find(x => x.key === saveData.mealId)?.text,
                    user: users.find(x => x.key === saveData.userId)?.text
                });
                setSaveInProgress(false);
            },
            (apiErrors) => {
                setSaveInProgress(false);
                setApiErrors(apiErrors);
            });
    }

    const onRenderFooterContent = () => {
        return (
            <>
                {
                    apiErrors &&
                    <MessageBar
                        messageBarType={MessageBarType.error}
                        isMultiline
                        truncated={!!apiErrors.length}
                        styles={{ root: { marginBottom: 5 } }}
                    >
                        {apiErrors}
                    </MessageBar>
                }
                {
                    saveInProgress ?
                        <ProgressIndicator
                            description="Saving food entry..."
                            barHeight={5}
                            styles={{ itemDescription: { marginRight: "15px" } }}
                        />
                        :
                        <div className={styles.buttons}>
                            <PrimaryButton onClick={saveEntry}>Save</PrimaryButton>
                            <DefaultButton onClick={cancel}>Cancel</DefaultButton>
                        </div>
                }
            </>
        )
    }

    return (
        <Panel
            headerText="Save food entry"
            isOpen={visible}
            onDismiss={cancel}
            onRenderFooterContent={onRenderFooterContent}
            isFooterAtBottom
        >
            <Dropdown 
                id="userId"
                placeholder="Select a user"
                label="User"
                defaultSelectedKey={data?.userId}
                options={users}
                required
                componentRef={input => { register(input, FieldTypes.Dropdown); }}
                errorMessage={errors.userId}
                disabled={saveInProgress}
            />
            <TextField
                id="name"
                label="Name"
                required
                maxLength={250}
                defaultValue={data?.name}
                componentRef={register}
                errorMessage={errors.name}
                disabled={saveInProgress}
            />
            <SpinButton
                id="calories"
                label="Calories"
                labelPosition={Position.top}
                min={1}
                step={1}
                value={String(calories)}
                onChange={(_, value) => { setCalories(value ? +value : 1); }}
                incrementButtonAriaLabel="Increase value by 1"
                decrementButtonAriaLabel="Decrease value by 1"
                disabled={saveInProgress}
            />
            <Dropdown
                id="mealId"
                placeholder="Select a meal"
                label="Meal"
                defaultSelectedKey={data?.mealId}
                options={meals}
                required
                componentRef={input => { register(input, FieldTypes.Dropdown); }}
                errorMessage={errors.mealId}
                disabled={saveInProgress}
            />
            <DatePicker
                id="created"
                label="Date"
                isRequired
                value={createdDate}
                onSelectDate={(date) => setCreatedDate(date || new Date())}
                formatDate={DateHelper.GetSimpleDateString}
                textField={{
                    disabled: saveInProgress,
                    required: true,
                    errorMessage: errors.created,
                    componentRef: (input) => register(input, FieldTypes.DatePicker)
                }}
                disabled={saveInProgress}
            />
        </Panel>
    )
};

export default SaveFoodEntryPanel;