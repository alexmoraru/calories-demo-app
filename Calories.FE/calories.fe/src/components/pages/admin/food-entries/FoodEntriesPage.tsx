import React, { useEffect, useState } from "react";
import { IColumn, IconButton, PrimaryButton, SelectionMode, ShimmeredDetailsList } from "@fluentui/react";

import FoodEntriesService from "../../../../common/api-services/FoodEntriesService";
import FoodEntryUserListViewOut from "../../../../common/models/api-models/FoodEntryUserListViewOut";
import SaveFoodEntryPanel from "./save-panel/SaveFoodEntryPanel";
import { useSnackbar } from "../../../common/snackbar-provider/SnackbarProvider";
import { NotificationType } from "../../../../common/models/SnackbarNotification";

import styles from "./FoodEntriesPage.module.scss";

const FoodEntriesPages: React.FC = () => {
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [entries, setEntries] = useState<FoodEntryUserListViewOut[]>([]);
    const [isPanelOpen, togglePanel] = useState<boolean>(false);
    const [editFoodEntry, setEditFoodEntry] = useState<FoodEntryUserListViewOut | undefined>(undefined);

    const { enqueueSnackbar } = useSnackbar();

    const columns: IColumn[] = [
        {
            key: "user",
            name: "User",
            fieldName: "user",
            minWidth: 150,
            maxWidth: 250
        },
        {
            key: "name",
            name: "Name",
            fieldName: "name",
            minWidth: 150,
            maxWidth: 250
        },
        {
            key: "calories",
            name: "Calories",
            fieldName: "calories",
            minWidth: 150,
            maxWidth: 250
        },
        {
            key: "meal",
            name: "Meal",
            fieldName: "meal",
            minWidth: 150,
            maxWidth: 250
        },
        {
            key: "created",
            name: "Created",
            fieldName: "created",
            minWidth: 150,
            maxWidth: 250,
            onRender: (item: FoodEntryUserListViewOut) => (
                <div>
                    {
                        new Date(item.created).toLocaleString("en-GB", {
                            day: "numeric",
                            month: "short",
                            year: "numeric"
                        })
                    }
                </div>
            )
        },
        {
            key: "actions",
            name: "",
            minWidth: 150,
            maxWidth: 250,
            onRender: (item: FoodEntryUserListViewOut) => (
                <div>
                    <IconButton iconProps={{ iconName: "Edit" }} onClick={() => { setEditFoodEntry(item); togglePanel(true); }} />
                    <IconButton iconProps={{ iconName: "Delete" }} onClick={() => { remove(item.id); }} />
                </div>
            )
        }
    ];

    useEffect(() => {
        setIsLoading(true);

        FoodEntriesService.getAll().then(
            data => {
                setEntries(data);

                setIsLoading(false);
            },
            (errors) => {
                enqueueSnackbar({
                    text: errors,
                    type: NotificationType.error
                });
                setIsLoading(false);
            });
            // eslint-disable-next-line
    }, []);

    const onSave = (data: FoodEntryUserListViewOut) => {
        togglePanel(false);

        let newEntries = [];

        if (editFoodEntry) {
            const index = entries.findIndex(x => x.id === data.id);
            newEntries = [...entries];
            newEntries[index] = data;
            setEditFoodEntry(undefined);
        } else {
            newEntries = [...entries, data];
        }

        setEntries(newEntries);
    }

    const remove = (id: number) => {
        const confirmationResult = window.confirm("Are you sure you want to delete this entry?");

        if (!confirmationResult) {
            return;
        }

        FoodEntriesService.remove(id).then(
            () => {
                setEntries([...entries.filter(x => x.id !== id)]);
            },
            (errors) => {
                console.log(errors);
            });
    }

    return (
        <div>
            <PrimaryButton
                text="Add new entry"
                onClick={() => { togglePanel(true); }}
                className={styles.addButton}
            />
            <ShimmeredDetailsList
                columns={columns}
                items={entries}
                enableShimmer={isLoading}
                selectionMode={SelectionMode.none}
            />
            <SaveFoodEntryPanel
                visible={isPanelOpen}
                data={editFoodEntry}
                save={onSave}
                cancel={() => { togglePanel(false); setEditFoodEntry(undefined); }}
            />
        </div>
    )
};

export default FoodEntriesPages;