import React, { useState, createContext, useContext, ReactNode } from 'react';
import { v4 as uuid } from "uuid";

import { MessageBar, MessageBarType } from "@fluentui/react";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import SnackbarNotification from '../../../common/models/SnackbarNotification';

import styles from "./SnackbarProvider.module.scss";

interface SnackbarContextValue {
    enqueueSnackbar: (message: SnackbarNotification) => void;
    closeSnackbar: (key?: any) => void;
}

const SnackbarContext = createContext<SnackbarContextValue>({} as SnackbarContextValue);

const SnackbarProvider: React.FC<{ children: ReactNode }> = ({ children, }) => {
    const [snacks, setSnacks] = useState<SnackbarNotification[]>([]);

    const enqueueSnackbar = (snackBar: SnackbarNotification) => {
        setSnacks(snacks.concat([{ ...snackBar, id: uuid() }]));
    }

    const closeSnackbar = (msg: SnackbarNotification) => {
        setSnacks(snacks.filter(e => e.id !== msg.id))
    }

    const contextValue = {
        enqueueSnackbar: enqueueSnackbar,
        closeSnackbar: closeSnackbar,
    };

    return (
        <SnackbarContext.Provider value={contextValue}>
            {children}
            {<div className={styles.notificationContainer}>
                <TransitionGroup className={styles.notificationGroup}>
                    {
                        snacks.map(msg => (
                            <CSSTransition
                                key={msg.id}
                                timeout={500}
                                classNames="notificationItem">
                                <div>
                                    <MessageBar
                                        key={msg.id}
                                        messageBarType={MessageBarType[msg.type as keyof typeof MessageBarType]}
                                        onDismiss={() => closeSnackbar(msg)}>
                                        <div className={styles.notificationMessage}>{msg.text}</div>
                                    </MessageBar>
                                </div>
                            </CSSTransition>
                        ))
                    }
                </TransitionGroup>
            </div>}
        </SnackbarContext.Provider>
    );
};

export const useSnackbar = (): SnackbarContextValue => useContext(SnackbarContext);

export default SnackbarProvider;