import { useState } from "react";
import { Switch, Route, BrowserRouter as Router, NavLink, Redirect } from "react-router-dom";
import clsx from "clsx";
import { Icon, PrimaryButton } from "@fluentui/react";
import { useTheme } from "@fluentui/react";

import { UserType } from "../../../common/constants/UserType";
import LoginPage from "../../pages/login/LoginPage";
import UserFoodEntriesPages from "../../pages/user/food-entries/FoodEntriesPage";
import AdminFoodEntriesPage from "../../pages/admin/food-entries/FoodEntriesPage";
import StatisticsPage from "../../pages/admin/statistics/StatisticsPage";
import InviteFriendPage from "../../pages/invite-friend/InviteFriendPage";
import SnackbarProvider from "../snackbar-provider/SnackbarProvider";

interface IMenuItem {
    name?: string,
    iconName?: string,
    url: string,
}

const AppRouting: React.FC = () => {
    const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);
    const [userType, setUserType] = useState<UserType | undefined>(undefined);

    const theme = useTheme();

    const onLoggedIn = (type: UserType) => {
        setIsAuthenticated(true);
        setUserType(type);
    }

    const getMenuItems = (): IMenuItem[] => {
        if (userType == UserType.User) {
            return [
                {
                    name: "Food entries",
                    iconName: "ShoppingCart",
                    url: "/"
                },
                {
                    name: "Invite a friend",
                    iconName: "AddFriend",
                    url: "/invite-friend"
                }
            ];
        } else {
            return [
                {
                    name: "Food entries",
                    iconName: "ShoppingCart",
                    url: "/"
                },
                {
                    name: "Invite a friend",
                    iconName: "AddFriend",
                    url: "/invite-friend"
                },
                {
                    name: "Statistics",
                    iconName: "StackColumnChart",
                    url: "/statistics"
                }
            ];
        }
    }

    return (
        <div className="app">
            <SnackbarProvider>
                <Router>
                    {
                        isAuthenticated && !!userType ?
                            <div className="appContainer">
                                <div className="sidenav" style={{ backgroundColor: theme.palette.themePrimary }}>
                                    <div className="sidenavContent">
                                        <div>
                                            {getMenuItems().map(item => {
                                                return (
                                                    <NavLink
                                                        exact={true}
                                                        key={item.url}
                                                        activeClassName={clsx("menuItem", "selected")}
                                                        className="menuItem"
                                                        to={item.url}>
                                                        <Icon iconName={item.iconName} />
                                                        {item.name}
                                                    </NavLink>
                                                );
                                            })}
                                        </div>
                                        <PrimaryButton text="Logout" onClick={() => { window.location.href = window.location.origin; }} />
                                    </div>
                                </div>
                                <div className="content">
                                    <Switch>
                                        {
                                            userType == UserType.User ?
                                                <>
                                                    <Route key="food-entries" exact path="/" component={UserFoodEntriesPages} />
                                                    <Route key="invite-friend" path="/invite-friend" component={InviteFriendPage} />
                                                    <Redirect to="/" />
                                                </>
                                                :
                                                <>
                                                    <Route key="food-entries" exact path="/" component={AdminFoodEntriesPage} />
                                                    <Route key="statistics" path="/statistics" component={StatisticsPage} />
                                                    <Route key="invite-friend" exact path="/invite-friend" component={InviteFriendPage} />
                                                    <Redirect to="/" />
                                                </>
                                        }
                                    </Switch>
                                </div>
                            </div>
                            :
                            <Switch>
                                <Route render={() => <LoginPage onLoggedIn={onLoggedIn} />}></Route>
                            </Switch>
                    }
                </Router >
            </SnackbarProvider>
        </div >
    );
};

export default AppRouting;