// import { MaskedTextField } from "@fluentui/react/lib/TextField";
// import { useTranslation } from "react-i18next";
import { matchesInteger, matchesNumeric, matchesUrl, matchesEmail } from "../../../common/utils/StringUtils";

export enum FieldTypes {
    Text = 1,
    Dropdown = 2,
    Integer = 3,
    Numeric = 4,
    Email = 5,
    Web = 6,
    Checkbox = 7,
    FileUploader = 8,
    DatePicker = 9,
    ChoiceGroup = 10,
    SpinButton = 11
}

interface IFieldData {
    fieldRef: any,
    type: FieldTypes,
    id: string,
    suffix?: string
}

const hasSuffix = (value: string, unitSuffix: string): Boolean => {
    const valueAsString = value.toString();
    const subString = valueAsString.substr(valueAsString.length - unitSuffix.length);
    return subString === unitSuffix;
};

const removeSuffix = (value: string, unitSuffix: string): string => {
    if (!hasSuffix(value, unitSuffix)) {
        return value;
    }
    return value.substr(0, value.length - unitSuffix.length);
};

const useOfficeForms = () => {
    const fields: IFieldData[] = [];

    const register = (element: any, type?: FieldTypes | null): void => {
        if (element) {
            fields.push({
                fieldRef: element,
                id: getElementIdByType(element, type),
                type: type ? type : FieldTypes.Text
            });
        }
    }

    const registerWithSuffix = (element: any, suffix: string, type?: FieldTypes | null): void => {
        if (element) {
            fields.push({
                fieldRef: element,
                id: getElementIdByType(element, type),
                type: type ? type : FieldTypes.Text,
                suffix: suffix
            });
        }
    }

    // const getMaskedFieldValue = (fieldRef: any) => {
    //     if (fieldRef.value)
    //         return fieldRef.value;
    //     else {
    //         let result = "";
    //         fieldRef._maskCharData.forEach((element: any) => {
    //             if (element.value)
    //                 result += element.value;
    //         });
    //         return result;
    //     }
    // }

    const validate = () => {
        let errors: any = {};
        let values: any = {};

        fields.forEach(field => {
            validateField(field, errors);
            updateFieldValue(field, values);
        });

        return {
            isFormValid: Object.keys(errors).length === 0,
            errors,
            values
        };
    }

    const getElementIdByType = (element: any, type?: FieldTypes | null) => {
        if (type === FieldTypes.FileUploader)
            return element.current ? element.current.id : undefined;
        return element.props.id;
    }

    const validateField = (field: any, errors: any) => {
        if (field.fieldRef && field.fieldRef.props && field.fieldRef.props.disabled) {
            return;
        }

        switch (field.type) {
            case FieldTypes.Dropdown: {
                if (field.fieldRef.props.required && (!field.fieldRef.selectedOptions || field.fieldRef.selectedOptions.length === 0)) {
                    errors[field.id] = "Required";
                }
                break;
            }
            case FieldTypes.Numeric: {
                let maxLength;
                if (field.fieldRef.props.maxLength !== undefined) {
                    maxLength = field.fieldRef.value < 0 ? field.fieldRef.props.maxLength + 1 : field.fieldRef.props.maxLength;
                }
                if (field.fieldRef.value.includes('.')) {
                    maxLength += 1;
                }
                if (field.fieldRef.props.required && !field.fieldRef.value) {
                    errors[field.id] = "Required";
                }
                else if (field.fieldRef.value && !matchesNumeric(field.fieldRef.value)) {
                    errors[field.id] = "Invalid numeric";
                }
                else if ((field.fieldRef.props.minLength !== undefined && field.fieldRef.props.maxLength !== undefined) && (field.fieldRef.props.minLength > field.fieldRef.value.length || maxLength < field.fieldRef.value.length)) {
                    errors[field.id] = "Value should be between {0} and {1}"
                        .replace("{0}", field.fieldRef.props.minLength)
                        .replace("{1}", field.fieldRef.props.maxLength);
                }
                else if (field.fieldRef.props.minLength > field.fieldRef.value.length) {
                    errors[field.id] = "Value should be greater than {0}"
                        .replace("{0}", field.fieldRef.props.minLength);
                }
                else if (maxLength < field.fieldRef.value.length) {
                    errors[field.id] = "Value should be lower than {0}"
                        .replace("{0}", field.fieldRef.props.maxLength);
                }
                else if ((field.fieldRef.props.min !== undefined && field.fieldRef.props.max !== undefined) && (field.fieldRef.props.min > field.fieldRef.value || field.fieldRef.props.max < field.fieldRef.value)) {
                    errors[field.id] = "Value should be between {0} and {1}"
                        .replace("{0}", field.fieldRef.props.min)
                        .replace("{1}", field.fieldRef.props.max);
                }
                else if (field.fieldRef.props.min > field.fieldRef.value) {
                    errors[field.id] = "Value should be lower than {0}"
                        .replace("{0}", field.fieldRef.props.min);
                }
                else if (field.fieldRef.props.max < field.fieldRef.value) {
                    errors[field.id] = "Value should be greater than {0}"
                        .replace("{0}", field.fieldRef.props.max);
                }
                break;
            }
            case FieldTypes.Integer: {
                let maxLength;
                if (field.fieldRef.props.maxLength !== undefined)
                    maxLength = field.fieldRef.value < 0 ? field.fieldRef.props.maxLength + 1 : field.fieldRef.props.maxLength;

                if (field.fieldRef.props.required && !field.fieldRef.value) {
                    errors[field.id] = "Required";
                }
                else if (field.fieldRef.value && !matchesInteger(field.fieldRef.value)) {
                    errors[field.id] = "Invalid integer";
                }
                else if ((field.fieldRef.props.minLength !== undefined && field.fieldRef.props.maxLength !== undefined) && (field.fieldRef.props.minLength > field.fieldRef.value.length || maxLength < field.fieldRef.value.length)) {
                    errors[field.id] = "Value should be between {0} and {1}"
                        .replace("{0}", field.fieldRef.props.minLength)
                        .replace("{1}", field.fieldRef.props.maxLength);
                }
                else if (field.fieldRef.props.minLength > field.fieldRef.value.length) {
                    errors[field.id] = "Value should be lower than {0}"
                        .replace("{0}", field.fieldRef.props.minLength);
                }
                else if (maxLength < field.fieldRef.value.length) {
                    errors[field.id] = "Value should be greater than {0}"
                        .replace("{0}", field.fieldRef.props.maxLength);
                }
                else if ((field.fieldRef.props.min !== undefined && field.fieldRef.props.max !== undefined) && (field.fieldRef.props.min > field.fieldRef.value || field.fieldRef.props.max < field.fieldRef.value)) {
                    errors[field.id] = "Value should be between {0} and {1}"
                        .replace("{0}", field.fieldRef.props.min)
                        .replace("{1}", field.fieldRef.props.max);
                }
                else if (field.fieldRef.props.min > field.fieldRef.value) {
                    errors[field.id] ="Value should be lower than {0}"
                        .replace("{0}", field.fieldRef.props.min);
                }
                else if (field.fieldRef.props.max < field.fieldRef.value) {
                    errors[field.id] = "Value should be greater than {0}"
                        .replace("{0}", field.fieldRef.props.max);
                }
                break;
            }
            case FieldTypes.Email: {
                if (field.fieldRef.props.required && !field.fieldRef.value) {
                    errors[field.id] = "Required";
                } else if (field.fieldRef.value && !matchesEmail(field.fieldRef.value)) {
                    errors[field.id] = "Invalid email";
                }
                break;
            }
            case FieldTypes.Web: {
                if (field.fieldRef.props.required && !field.fieldRef.value) {
                    errors[field.id] = "Required";
                }
                else if (field.fieldRef.value && !matchesUrl(field.fieldRef.value)) {
                    errors[field.id] = "Invalid website";
                }
                break;
            }
            case FieldTypes.FileUploader: {
                if (field.fieldRef.current.required && !field.fieldRef.current.validity.valid) {
                    errors[field.fieldRef.current.id] = "Required";
                }
                break;
            }
            case FieldTypes.DatePicker: {
                const value = field.fieldRef.value;

                if (field.fieldRef.props.required && (!value || value.trim() === "")) {
                    errors[field.id.replace("-label", "")] = "Required";
                }

                break;
            }
            case FieldTypes.ChoiceGroup: {
                if (field.fieldRef.props.required && !(field.fieldRef.checkedOption && field.fieldRef.checkedOption.key)) {
                    errors[field.id] = "Required";
                }
                break;
            }
            default: {
                let value = "";
                // if (field.fieldRef instanceof MaskedTextField) {
                //     value = getMaskedFieldValue(field.fieldRef);
                // } else {
                    value = field.fieldRef.value;
                // }
                if (field.fieldRef.props.required && (!value || value?.trim() === "")) {
                    errors[field.id] = "Required";
                } else if (field.fieldRef.props.minLength && value.trim().length < field.fieldRef.props.minLength) {
                    errors[field.id] = "Text should be longer than {0}"
                        .replace("{0}", field.fieldRef.props.minLength);
                }
                break;
            }
        }
    };

    const updateFieldValue = (field: IFieldData, values: any) => {
        switch (field.type) {
            case FieldTypes.Text: {
                let value = "";
                // if (field.fieldRef instanceof MaskedTextField) {
                //     value = getMaskedFieldValue(field.fieldRef);
                // } else {
                    value = field.fieldRef.value;
                // }

                if (field.suffix) {
                    value = removeSuffix(value, field.suffix);
                }

                values[field.id] = value?.trim();
                break;
            }
            case FieldTypes.Dropdown: {
                if (field.fieldRef.selectedOptions.length)
                    values[field.id] = field.fieldRef.selectedOptions[0].key;
                break;
            }
            case FieldTypes.Checkbox: {
                values[field.id] = field.fieldRef.checked;
                break;
            }
            case FieldTypes.FileUploader: {
                values[field.fieldRef.current.id] = field.fieldRef.current.defaultValue;
                break;
            }
            case FieldTypes.ChoiceGroup: {
                if (field.fieldRef.checkedOption) {
                    values[field.id] = field.fieldRef.checkedOption.key;
                }
                break;
            }
            case FieldTypes.DatePicker: {
                values[field.id.replace("-label", "")] = field.fieldRef.value;
                break;
            }
            case FieldTypes.SpinButton: {
                let value = field.fieldRef.value;
                if (field.suffix) {
                    value = removeSuffix(value, field.suffix);
                }
                values[field.id] = parseFloat(value.trim());
                break;
            }
            default: {
                values[field.id] = field.fieldRef.value;
                break;
            }
        }
    };

    return {
        register,
        registerWithSuffix,
        validate
    }
};



export default useOfficeForms;