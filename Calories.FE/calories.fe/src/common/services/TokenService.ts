class TokenService {
    private authToken: string;
    private static instance: TokenService;

    private constructor() {
        this.authToken = "";
    }

    public static get Instance() {
        return this.instance || (this.instance = new this());
    }

    public setToken(token: string) {
        this.authToken = token;
    }

    public getToken(): string {
        return this.authToken;
    }
}

export default TokenService.Instance;

