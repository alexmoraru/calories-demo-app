export default class ArrayHelper {
    public static sort(arr: any[], propertyName: string, ascending: boolean): any[] {
        const arrJson = JSON.stringify(arr);
        const newArr = JSON.parse(arrJson);
        if (ascending) {
            return newArr.sort((a: any, b: any) => {
                let valueA, valueB;
                if (a[propertyName]["toUpperCase"]) {
                    valueA = a[propertyName].toUpperCase();
                    valueB = b[propertyName].toUpperCase();
                } else {
                    valueA = a[propertyName];
                    valueB = b[propertyName];
                }

                return valueA > valueB ? 1 : ((valueB > valueA) ? -1 : 0);
            });
        } else {
            return newArr.sort((a: any, b: any) => {
                let valueA, valueB;
                if (a[propertyName]["toUpperCase"]) {
                    valueA = a[propertyName].toUpperCase();
                    valueB = b[propertyName].toUpperCase();
                } else {
                    valueA = a[propertyName];
                    valueB = b[propertyName];
                }

                return valueA > valueB ? -1 : ((valueB > valueA) ? 1 : 0);
            });
        }
    }
}