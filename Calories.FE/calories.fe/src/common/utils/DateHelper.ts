export default class DateHelper {
    public static GetSimpleDateString(date?: Date): string {
        return date!.getFullYear() + "-" + (date!.getMonth() < 9 ? ("0" + (date!.getMonth() + 1)) : (date!.getMonth() + 1)) + "-" + (date!.getDate() < 10 ? ("0" + date!.getDate()) : date!.getDate());
    }
}