export enum NotificationType {
    blocked = "blocked",
    error = "error",
    info = "info",
    remove = "remove",
    severeWarning = "severeWarning",
    success = "success",
    warning = "warning"
}
export default interface SnackbarNotification {
    id?: string,
    type: NotificationType,
    text: string,
    timeout?: number
}