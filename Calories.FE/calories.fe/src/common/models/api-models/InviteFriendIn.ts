export default interface InviteFriendIn {
    name: string;

    email: string;
}