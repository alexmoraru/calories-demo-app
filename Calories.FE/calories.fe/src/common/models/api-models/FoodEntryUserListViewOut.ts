import FoodEntryListViewOut from "./FoodEntryListViewOut";

export default interface FoodEntryUserListViewOut extends FoodEntryListViewOut {
    mealId: number;

    userId: number;

    user: string;
}