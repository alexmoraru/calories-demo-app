export default interface FoodEntriesStatisticsAverageOut {
    userId: number;

    userName: string;

    calories: number;
}