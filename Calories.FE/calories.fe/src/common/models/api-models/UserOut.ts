export default interface UserOut {
    id: number;

    name: string;

    email: string;
}