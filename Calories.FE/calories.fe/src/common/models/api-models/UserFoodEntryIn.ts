import FoodEntryIn from "./FoodEntryIn";

export default interface UserFoodEntryIn extends FoodEntryIn {
    userId: number;
}