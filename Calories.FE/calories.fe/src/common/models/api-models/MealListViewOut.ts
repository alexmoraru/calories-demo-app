export default interface MealListViewOut {
    id: number;
    name: string;
}