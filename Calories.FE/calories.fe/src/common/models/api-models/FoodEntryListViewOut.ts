export default interface FoodEntryListViewOut {
    id: number;

    name: string;

    calories: number;

    created: Date;
}