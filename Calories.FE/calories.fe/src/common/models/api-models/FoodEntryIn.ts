export default interface FoodEntryIn {
    name: string;

    calories: number;

    mealId: number;

    created: Date;
}