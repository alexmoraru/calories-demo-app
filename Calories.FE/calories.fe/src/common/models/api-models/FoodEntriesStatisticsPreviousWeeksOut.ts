export default interface FoodEntriesStatisticsPreviousWeeksOut {
    lastWeek: number;

    previousToLastWeek: number;
}