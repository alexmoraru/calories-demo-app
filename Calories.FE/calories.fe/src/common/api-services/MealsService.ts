import Api from "./AxiosApi";
import MealListViewOut from "../models/api-models/MealListViewOut";

export class MealsService {
    public get(): Promise<MealListViewOut[]> {
        return Api.get("meals");
    }
}

export default new MealsService();