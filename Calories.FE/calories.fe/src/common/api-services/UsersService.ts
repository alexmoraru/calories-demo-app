import Api from "./AxiosApi";
import InviteFriendIn from "../models/api-models/InviteFriendIn";
import InviteFriendOut from "../models/api-models/InviteFriendOut";
import UserOut from "../models/api-models/UserOut";

export class UsersService {
    public getToken(email: string): Promise<string> {
        return Api.get(`users/token/${email}`);
    }

    public getCaloriesLimit(): Promise<number> {
        return Api.get("users/caloriesLimit");
    }

    public getAll(): Promise<UserOut[]> {
        return Api.get("users");
    }

    public invite(data: InviteFriendIn): Promise<InviteFriendOut> {
        return Api.post("users/invite", data);
    }
}

export default new UsersService();