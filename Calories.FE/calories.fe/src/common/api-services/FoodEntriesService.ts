import Api from "./AxiosApi";
import FoodEntryListViewOut from "../models/api-models/FoodEntryListViewOut";
import FoodEntryUserListViewOut from "../models/api-models/FoodEntryUserListViewOut";
import FoodEntryIn from "../models/api-models/FoodEntryIn";
import FoodEntriesStatisticsPreviousWeeksOut from "../models/api-models/FoodEntriesStatisticsPreviousWeeksOut";
import FoodEntriesStatisticsAverageOut from "../models/api-models/FoodEntriesStatisticsAverageOut";
import UserFoodEntryIn from "../models/api-models/UserFoodEntryIn";

export class FoodEntriesService {
    public get(): Promise<FoodEntryListViewOut[]> {
        return Api.get(`foodEntries`);
    }

    public getAll(): Promise<FoodEntryUserListViewOut[]> {
        return Api.get("foodEntries/all");
    }

    public getWeeksStatistics(): Promise<FoodEntriesStatisticsPreviousWeeksOut> {
        return Api.get("foodEntries/statistics/previousWeeks");
    }

    public getAverageStatistics(): Promise<FoodEntriesStatisticsAverageOut[]> {
        return Api.get("foodEntries/statistics/average");
    }

    public add(data: FoodEntryIn): Promise<number> {
        return Api.post('foodEntries', data);
    }

    public addForUser(data: UserFoodEntryIn): Promise<number> {
        return Api.post("foodEntries/user", data);
    }

    public update(id: number, data: UserFoodEntryIn): Promise<any> {
        return Api.put(`foodEntries/${id}/user`, data);
    }

    public remove(id: number): Promise<any> {
        return Api.delete(`foodEntries/${id}`);
    }
}

export default new FoodEntriesService();