import axios from "axios";
import TokenService from "../services/TokenService";

const instance = axios.create({
    baseURL: process.env.REACT_APP_BASE_API_URL
});

instance.interceptors.request.use(
    async (config) => {

        const token: string = TokenService.getToken();

        if (config && config.headers) {
            config.headers["Authorization"] = token;
        }

        return config;
    }
);

instance.interceptors.response.use(
    (response) => {
        if (response) {
            return response.data;
        }

        return response;
    },
    (error) => {
        if (error && error.response.data.errors) {
            return Promise.reject(error.response.data.errors);
        }

        return Promise.reject(error);
    }
);

export default instance;