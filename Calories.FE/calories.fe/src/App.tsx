import React from 'react';

import AppRouting from './components/common/routing/AppRouting';

import './App.scss';

const App: React.FC = () => {
  return (
    <AppRouting />
  );
}

export default App;
