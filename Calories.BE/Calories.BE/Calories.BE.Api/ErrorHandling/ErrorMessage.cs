﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Calories.BE.Api.ErrorHandling
{
    public class ErrorMessage
    {
        public List<string> Errors { get; set; }

        public string Exception { get; set; }

        public int ErrorCode { get; set; }

        public ErrorMessage()
        {
            Errors = new List<string>();
            Exception = string.Empty;
            ErrorCode = -1;
        }

        public ErrorMessage(Exception ex)
        {
            Errors = new List<string> { ex.Message };
            Exception = ex.GetType().Name;
            ErrorCode = (int)HttpStatusCode.InternalServerError;
        }

        public ErrorMessage(string message, string exceptionType)
        {
            Errors = new List<string> { message };
            Exception = exceptionType;
            ErrorCode = (int)HttpStatusCode.InternalServerError;
        }

        public ErrorMessage(ValidationException ex)
        {
            Errors = ex.Errors.Select(x => x.ErrorMessage).ToList();
            Exception = ex.GetType().Name;
            ErrorCode = (int)HttpStatusCode.InternalServerError;
        }
    }
}
