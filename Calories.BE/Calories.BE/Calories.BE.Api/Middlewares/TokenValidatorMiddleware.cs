﻿using Calories.BE.Domain.Exceptions;
using Calories.BE.Manager.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace Calories.BE.Api.Middlewares
{
    public class TokenValidatorMiddleware
    {
        private readonly RequestDelegate request;
        private readonly ILogger<TokenValidatorMiddleware> logger;

        public TokenValidatorMiddleware(RequestDelegate next, ILogger<TokenValidatorMiddleware> logger)
        {
            request = next;
            this.logger = logger;
        }

        public Task Invoke(HttpContext context) => InvokeAsync(context);

        private async Task InvokeAsync(HttpContext context)
        {
            if (context.Request.Headers.ContainsKey("Authorization"))
            {
                var authHeaderValue = context.Request.Headers["Authorization"][0];
                authHeaderValue = authHeaderValue.Replace("Bearer ", string.Empty);

                if (!TokenHelper.ValidateToken(authHeaderValue))
                {
                    throw new AuthException("Invalid token");
                }

                var identity = new GenericIdentity(TokenHelper.GetClaim(authHeaderValue, "nameid"));
                identity.AddClaim(new Claim("Id", TokenHelper.GetClaim(authHeaderValue, "nameid")));
                identity.AddClaim(new Claim("Role", TokenHelper.GetClaim(authHeaderValue, "role")));

                context.User = new GenericPrincipal(identity, new[] { TokenHelper.GetClaim(authHeaderValue, "role") });

                await request(context);
            }
            else
            {
                var endpoint = context.Features.Get<IEndpointFeature>()?.Endpoint;
                var attribute = endpoint?.Metadata.GetMetadata<AllowAnonymousAttribute>();
                if (attribute == null)
                {
                    logger.LogError(new AuthException("No Authorization header found for this request"), "");

                    throw new AuthException("No Authorization header found for this request");
                }
                else
                {
                    await request(context);
                }
            }
        }
    }
}
