﻿using Calories.BE.Api.ErrorHandling;
using Calories.BE.Domain.Exceptions;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Calories.BE.Api.Middlewares
{
    public class ExceptionHandlerMiddleware
    {
        private const string JsonContentType = "application/json";
        private readonly RequestDelegate _request;
        private readonly ILogger<ExceptionHandlerMiddleware> _logger;

        public ExceptionHandlerMiddleware(RequestDelegate next, ILogger<ExceptionHandlerMiddleware> logger)
        {
            _request = next;
            _logger = logger;
        }

        public Task Invoke(HttpContext context) => InvokeAsync(context);

        async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _request(context);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Empty);

                var httpStatusCode = ConfigurateExceptionTypes(ex);
                var customErrorMessage = HandleExceptionType(ex);
                customErrorMessage.ErrorCode = httpStatusCode;

                context.Response.StatusCode = httpStatusCode;
                context.Response.ContentType = JsonContentType;

                await context.Response.WriteAsync(
                    JsonConvert.SerializeObject(
                        customErrorMessage,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        }));
            }
        }

        private static int ConfigurateExceptionTypes(Exception exception)
        {
            int httpStatusCode;

            switch (exception)
            {
                case EntityNotFoundException _:
                    httpStatusCode = (int)HttpStatusCode.NotFound;
                    break;
                case AuthException _:
                    httpStatusCode = (int)HttpStatusCode.Unauthorized;
                    break;
                case UnauthorizedAccessException _:
                    httpStatusCode = (int)HttpStatusCode.Unauthorized;
                    break;
                //validation exceptions
                default:
                    httpStatusCode = (int)HttpStatusCode.InternalServerError;
                    break;
            }

            return httpStatusCode;
        }

        private ErrorMessage HandleExceptionType(Exception exception)
        {
            ErrorMessage message;
            const string defaultExceptionKey = "An error has occured";

            //handle exception types
            switch (exception)
            {
                case ManagerException managerException:
                    message = new ErrorMessage(managerException.Message, exception.GetType().Name);
                    break;

                case EntityNotFoundException notFoundException:
                    message = new ErrorMessage(notFoundException.Message, exception.GetType().Name);
                    break;

                case ValidationException validationException:
                    message = new ErrorMessage(validationException);
                    break;

                case AuthException authException:
                    message = new ErrorMessage(authException);
                    break;

                default:
                    message = new ErrorMessage(new Exception(defaultExceptionKey));
                    break;
            }

            return message;
        }
    }
}
