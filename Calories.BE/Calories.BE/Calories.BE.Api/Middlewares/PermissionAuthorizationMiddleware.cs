﻿using Calories.BE.Domain.Attributes;
using Calories.BE.Domain.Constants;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Calories.BE.Api.Middlewares
{
    public class PermissionAuthorizationMiddleware
    {
        private readonly RequestDelegate request;
        private readonly ILogger<PermissionAuthorizationMiddleware> logger;

        public PermissionAuthorizationMiddleware(RequestDelegate next, ILogger<PermissionAuthorizationMiddleware> logger)
        {
            request = next;
            this.logger = logger;
        }

        public Task Invoke(HttpContext context) => InvokeAsync(context);

        private async Task InvokeAsync(HttpContext context)
        {
            var endpoint = context.Features.Get<IEndpointFeature>()?.Endpoint;
            var attribute = endpoint?.Metadata.GetMetadata<AdminRoleAttribute>();
            if (attribute != null && !context.User.IsInRole(((int)UserRole.Admin).ToString()))
            {
                logger.LogError("Attempt to call an admin endpoint without proper permissions");

                throw new UnauthorizedAccessException();
            }
            else
            {
                await request(context);
            }
        }
    }
}
