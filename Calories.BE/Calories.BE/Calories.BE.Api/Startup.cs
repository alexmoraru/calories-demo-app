using Calories.BE.Api.Filters;
using Calories.BE.Api.Middlewares;
using Calories.BE.Data;
using Calories.BE.Data.Interfaces;
using Calories.BE.Data.Repositories;
using Calories.BE.Domain.Mapper;
using Calories.BE.Domain.Settings;
using Calories.BE.Manager.Services;
using Calories.BE.Manager.Validators;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Calories.BE.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddControllers(opt =>
                {
                    opt.Filters.Add(typeof(ValidatorActionFilter));
                })
                .AddNewtonsoftJson()
                .AddFluentValidation(fv =>
                {
                    fv.RegisterValidatorsFromAssemblyContaining<FoodEntryInValidator>();
                    fv.DisableDataAnnotationsValidation = true;
                });

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddCors(options =>
            {
                options.AddPolicy("MyCors",
                builder =>
                {
                    builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
                });
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Calories.BE.Api", Version = "v1" });
            });

            services.Configure<AppConfigData>(Configuration.GetSection("Settings"));

            services.AddAutoMapper(typeof(AppMapperConfigProfile));

            services.AddSingleton<DapperContext>();
            services.AddScoped<IFoodEntriesRepository, FoodEntriesRepository>();
            services.AddScoped<IUsersRepository, UsersRepository>();
            services.AddScoped<IMealsRepository, MealsRepository>();

            services.AddScoped<FoodEntriesService>();
            services.AddScoped<UsersService>();
            services.AddScoped<MealsService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Calories.BE.Api v1"));
            }

            app.UseHttpsRedirection();

            app.UseCors("MyCors");

            app.UseRouting();

            app.UseMiddleware<ExceptionHandlerMiddleware>();
            app.UseMiddleware<TokenValidatorMiddleware>();
            app.UseMiddleware<PermissionAuthorizationMiddleware>();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
