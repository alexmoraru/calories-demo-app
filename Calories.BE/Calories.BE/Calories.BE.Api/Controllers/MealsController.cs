﻿using Calories.BE.Domain.ApiModels.Out;
using Calories.BE.Manager.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calories.BE.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MealsController : ControllerBase
    {
        private readonly MealsService service;

        public MealsController(MealsService service)
        {
            this.service = service;
        }

        [HttpGet]
        public Task<List<MealListViewOut>> Get()
        {
            return service.Get();
        }
    }
}
