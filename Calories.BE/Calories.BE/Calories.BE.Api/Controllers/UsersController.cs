﻿using Calories.BE.Domain.ApiModels.In;
using Calories.BE.Domain.ApiModels.Out;
using Calories.BE.Manager.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calories.BE.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : BaseAppController
    {
        private readonly UsersService service;

        public UsersController(UsersService service)
        {
            this.service = service;
        }

        [HttpGet("token/{email}")]
        [AllowAnonymous]
        public Task<string> GetToken(string email)
        {
            return service.GetToken(email);
        }

        [HttpGet("caloriesLimit")]
        public Task<int> GetCaloriesLimit()
        {
            return service.GetCaloriesLimit(GetCurrentUserIdentifier());
        }

        [HttpGet]
        public Task<List<UserOut>> Get()
        {
            return service.GetAll();
        }

        [HttpPost("invite")]
        public Task<InviteFriendOut> Invite(InviteFriendIn input)
        {
            return service.Invite(input);
        }
    }
}
