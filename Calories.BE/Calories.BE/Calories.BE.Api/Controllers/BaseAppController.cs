﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Calories.BE.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class BaseAppController : ControllerBase
    {
        protected int GetCurrentUserIdentifier()
        {
            return Convert.ToInt32(HttpContext.User.FindFirst("Id").Value);
        }
    }
}
