﻿using Calories.BE.Domain.ApiModels.In;
using Calories.BE.Domain.ApiModels.Out;
using Calories.BE.Domain.Attributes;
using Calories.BE.Manager.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calories.BE.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoodEntriesController : BaseAppController
    {
        private readonly FoodEntriesService service;

        public FoodEntriesController(FoodEntriesService service)
        {
            this.service = service;
        }

        [HttpGet]
        public Task<List<FoodEntryListViewOut>> GetEntries()
        {
            return service.Get(GetCurrentUserIdentifier());
        }

        [HttpGet("all")]
        [AdminRole]
        public Task<List<FoodEntryUserListViewOut>> GetAllEntries()
        {
            return service.Get();
        }

        [HttpGet("statistics/previousWeeks")]
        [AdminRole]
        public Task<FoodEntriesStatisticsPreviousWeeksOut> GetStatisticsPreviousWeeks()
        {
            return service.GetStatisticsPreviousWeeks();
        }

        [HttpGet("statistics/average")]
        [AdminRole]
        public Task<List<FoodEntriesStatisticsAverageOut>> GetStatisticsAverage()
        {
            return service.GetStatisticsAverage();
        }

        [HttpPost]
        public Task<long> Add(FoodEntryIn input)
        {
            return service.Add(GetCurrentUserIdentifier(), input);
        }

        [HttpPost("user")]
        [AdminRole]
        public Task<long> Add(UserFoodEntryIn input)
        {
            return service.Add(input);
        }

        [HttpPut("{id}/user")]
        [AdminRole]
        public Task Update(long id, UserFoodEntryIn input)
        {
            return service.Update(id, input);
        }

        [HttpDelete("{id}")]
        [AdminRole]
        public Task Delete(long id)
        {
            return service.Delete(id);
        }
    }
}
