﻿using Calories.BE.Data.Abstract;
using Calories.BE.Data.Interfaces;
using Calories.BE.Domain.ApiModels.Out;
using Calories.BE.Domain.DbModels;
using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calories.BE.Data.Repositories
{
    public class FoodEntriesRepository : AppDbRepository, IFoodEntriesRepository
    {
        public FoodEntriesRepository(DapperContext context) : base(context)
        {
        }

        public async Task<long> Add(FoodEntry model)
        {
            using (var connection = Context.CreateConnection())
            {
                return await connection.InsertAsync(model);
            }
        }

        public async Task<List<FoodEntry>> Get(long userId)
        {
            using (var connection = Context.CreateConnection())
            {
                return (await connection.QueryAsync<FoodEntry, Meal, FoodEntry>(
                    $@"SELECT fe.*, meals.Id as FoodEntryMealId, meals.* FROM {GetTableName<FoodEntry>()} fe
                    INNER JOIN {GetTableName<Meal>()} meals ON meals.{GetColumnName<Meal>(x => x.Id)} = fe.{GetColumnName<FoodEntry>(x => x.MealId)}
                    WHERE {GetColumnName<FoodEntry>(x => x.UserId)} = @userId
                    ORDER BY fe.{GetColumnName<FoodEntry>(x => x.Created)}, fe.{GetColumnName<FoodEntry>(x => x.MealId)}",
                    (foodEntry, meal) =>
                    {
                        foodEntry.Meal = meal;

                        return foodEntry;
                    },
                    new { userId },
                    splitOn: "FoodEntryMealId")).ToList();
            }
        }

        public async Task<List<FoodEntry>> Get()
        {
            using (var connection = Context.CreateConnection())
            {
                return (await connection.QueryAsync<FoodEntry, Meal, User, FoodEntry>(
                    $@"SELECT fe.*, meals.Id as FoodEntryMealId, meals.*, users.Id AS FoodEntryUserId, users.* FROM {GetTableName<FoodEntry>()} fe
                    INNER JOIN {GetTableName<Meal>()} meals ON meals.{GetColumnName<Meal>(x => x.Id)} = fe.{GetColumnName<FoodEntry>(x => x.MealId)}
                    INNER JOIN {GetTableName<User>()} users ON users.{GetColumnName<User>(x => x.Id)} = fe.{GetColumnName<FoodEntry>(x => x.UserId)}
                    ORDER BY fe.{GetColumnName<FoodEntry>(x => x.Created)}, fe.{GetColumnName<FoodEntry>(x => x.MealId)}, users.{GetColumnName<User>(x => x.Name)}",
                    (foodEntry, meal, user) =>
                    {
                        foodEntry.Meal = meal;
                        foodEntry.User = user;

                        return foodEntry;
                    },
                    splitOn: "FoodEntryMealId,FoodEntryUserId")).ToList();
            }
        }

        public async Task<int> GetCount(long userId, long mealId, DateTime date)
        {
            using (var connection = Context.CreateConnection())
            {
                return await connection.QueryFirstAsync<int>($@"SELECT COUNT(*) FROM {GetTableName<FoodEntry>()} 
                    WHERE {GetColumnName<FoodEntry>(x => x.UserId)} = @userId AND 
                        {GetColumnName<FoodEntry>(x => x.MealId)} = @mealId AND 
                        {GetColumnName<FoodEntry>(x => x.Created)} = @date",
                    new { userId, mealId, date });
            }
        }

        public async Task<int> GetCount(DateTime startDate, DateTime endDate)
        {
            using (var connection = Context.CreateConnection())
            {
                return await connection.QueryFirstAsync<int>($@"SELECT COUNT(*) FROM {GetTableName<FoodEntry>()} 
                    WHERE {GetColumnName<FoodEntry>(x => x.Created)} >= @startDate AND 
                        {GetColumnName<FoodEntry>(x => x.Created)} < @endDate",
                    new { startDate, endDate });
            }
        }

        public async Task Update(FoodEntry model)
        {
            using (var connection = Context.CreateConnection())
            {
                await connection.UpdateAsync(model);
            }
        }

        public async Task Delete(long id)
        {
            using (var connection = Context.CreateConnection())
            {
                await connection.DeleteAsync(new FoodEntry { Id = id });
            }
        }

        public async Task<List<FoodEntriesStatisticsAverageOut>> GetAverageStatistics(DateTime startDate)
        {
            using (var connection = Context.CreateConnection())
            {
                return (await connection.QueryAsync<FoodEntriesStatisticsAverageOut>(
                    $@"SELECT fe.{GetColumnName<FoodEntry>(x => x.UserId)}, users.{GetColumnName<User>(x => x.Name)} AS UserName, AVG(fe.Calories) AS Calories FROM {GetTableName<FoodEntry>()} fe
                    INNER JOIN {GetTableName<User>()} users ON users.{GetColumnName<User>(x => x.Id)} = fe.{GetColumnName<FoodEntry>(x => x.UserId)}
                    WHERE fe.{GetColumnName<FoodEntry>(x => x.Created)} >= @startDate
                    GROUP BY fe.{GetColumnName<FoodEntry>(x => x.UserId)}, users.{GetColumnName<User>(x => x.Name)}",
                    new { startDate }))
                    .ToList();
            }
        }
    }
}
