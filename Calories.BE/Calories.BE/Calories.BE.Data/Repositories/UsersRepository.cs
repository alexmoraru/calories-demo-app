﻿using Calories.BE.Data.Abstract;
using Calories.BE.Data.Interfaces;
using Calories.BE.Domain.Constants;
using Calories.BE.Domain.DbModels;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calories.BE.Data.Repositories
{
    public class UsersRepository : AppDbRepository, IUsersRepository
    {
        public UsersRepository(DapperContext context) : base(context)
        {
        }

        public async Task<long> Add(User model)
        {
            using (var connection = Context.CreateConnection())
            {
                return await connection.InsertAsync(model);
            }
        }

        public async Task<User> Get(string email)
        {
            using (var connection = Context.CreateConnection())
            {
                return await connection.QueryFirstOrDefaultAsync<User>($"SELECT * FROM {GetTableName<User>()} WHERE {GetColumnName<User>(x => x.Email)} = @email", new { email });
            }
        }

        public async Task<User> Get(long id)
        {
            using (var connection = Context.CreateConnection())
            {
                return await connection.GetAsync<User>(id);
            }
        }

        public async Task<List<User>> Get(UserRole role)
        {
            using (var connection = Context.CreateConnection())
            {
                return (await connection.QueryAsync<User>($"SELECT * FROM {GetTableName<User>()} WHERE {GetColumnName<User>(x => x.Role)} = @role", new { role })).ToList();
            }
        }
    }
}
