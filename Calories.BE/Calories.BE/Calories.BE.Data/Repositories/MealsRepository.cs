﻿using Calories.BE.Data.Abstract;
using Calories.BE.Data.Interfaces;
using Calories.BE.Domain.DbModels;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calories.BE.Data.Repositories
{
    public class MealsRepository : AppDbRepository, IMealsRepository
    {
        public MealsRepository(DapperContext context) : base(context)
        {
        }

        public async Task<List<Meal>> Get()
        {
            using (var connection = Context.CreateConnection())
            {
                return (await connection.GetAllAsync<Meal>()).ToList();
            }
        }

        public async Task<Meal> Get(long id)
        {
            using (var connection = Context.CreateConnection())
            {
                return await connection.GetAsync<Meal>(id);
            }
        }
    }
}
