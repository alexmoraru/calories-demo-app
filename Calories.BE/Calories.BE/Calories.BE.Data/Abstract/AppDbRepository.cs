﻿using Dapper.Contrib.Extensions;
using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Calories.BE.Data.Abstract
{
    public abstract class AppDbRepository
    {
        protected DapperContext Context { get; set; }

        protected AppDbRepository(DapperContext context)
        {
            Context = context;
        }

        protected static string GetColumnName<T>(Expression<Func<T, object>> selector)
        {
            //Because the type of the expression is Func<T,object> this means any property you select will be converted into an object (if it's not of type "object", which most probably it isn't)
            // so this means we need to get the inner expression of the convert expression
            var propertySelectorExpression = selector.Body;
            if (propertySelectorExpression is UnaryExpression && (propertySelectorExpression as UnaryExpression).NodeType == ExpressionType.Convert)
            {
                propertySelectorExpression = (propertySelectorExpression as UnaryExpression).Operand;
            }

            if (propertySelectorExpression is MemberExpression && (propertySelectorExpression as MemberExpression).Member is PropertyInfo)
            {
                var propertyInfo = (PropertyInfo)((MemberExpression)propertySelectorExpression).Member;
                var propertyName = propertyInfo.Name;

                var propertyAttributes = propertyInfo.GetCustomAttributes(true);
                foreach (var attr in propertyAttributes)
                {
                    var attrType = attr.GetType();
                    if (attrType.FullName != "Dapper.Contrib.Extensions.WriteAttribute")
                        continue;

                    var attrProperty = attrType.GetProperty("Write");
                    if (attrProperty != null)
                    {
                        var attrPropertyValue = attrProperty.GetValue(attr);
                        if (attrPropertyValue.GetType() == typeof(bool) && !(bool)attrPropertyValue)
                        {
                            throw new NotSupportedException("Must select a property that is mapped to one of the table columns.");
                        }
                    }
                }

                return propertyName;
            }
            throw new NotSupportedException("Must select a property of the model");
        }

        protected static string GetTableName<T>()
        {
            // Check if we've already set our custom table mapper to TableNameMapper.
            if (SqlMapperExtensions.TableNameMapper != null)
                return SqlMapperExtensions.TableNameMapper(typeof(T));

            // If not, we can use Dapper default method "SqlMapperExtensions.GetTableName(Type type)" which is unfortunately private, that's why we have to call it via reflection.
            string getTableName = "GetTableName";
            MethodInfo getTableNameMethod = typeof(SqlMapperExtensions).GetMethod(getTableName, BindingFlags.NonPublic | BindingFlags.Static);

            if (getTableNameMethod == null)
                throw new ArgumentOutOfRangeException($"Method '{getTableName}' is not found in '{nameof(SqlMapperExtensions)}' class.");

            return getTableNameMethod.Invoke(null, new object[] { typeof(T) }) as string;
        }
    }
}
