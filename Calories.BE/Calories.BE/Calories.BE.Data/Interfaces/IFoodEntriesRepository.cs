﻿using Calories.BE.Domain.ApiModels.Out;
using Calories.BE.Domain.DbModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calories.BE.Data.Interfaces
{
    public interface IFoodEntriesRepository
    {
        Task<List<FoodEntry>> Get(long userId);

        Task<List<FoodEntry>> Get();

        Task<int> GetCount(DateTime startDate, DateTime endDate);

        Task<int> GetCount(long userId, long mealId, DateTime date);

        Task<List<FoodEntriesStatisticsAverageOut>> GetAverageStatistics(DateTime startDate);

        Task<long> Add(FoodEntry model);

        Task Update(FoodEntry model);

        Task Delete(long id);
    }
}
