﻿using Calories.BE.Domain.Constants;
using Calories.BE.Domain.DbModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calories.BE.Data.Interfaces
{
    public interface IUsersRepository
    {
        Task<User> Get(string email);

        Task<User> Get(long id);

        Task<List<User>> Get(UserRole role);

        Task<long> Add(User model);
    }
}
