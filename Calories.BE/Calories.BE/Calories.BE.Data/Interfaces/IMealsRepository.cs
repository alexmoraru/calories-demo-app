﻿using Calories.BE.Domain.DbModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calories.BE.Data.Interfaces
{
    public interface IMealsRepository
    {
        Task<List<Meal>> Get();

        Task<Meal> Get(long id);
    }
}
