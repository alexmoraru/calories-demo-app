﻿using Calories.BE.Domain.Settings;
using Microsoft.Extensions.Options;
using System.Data;
using System.Data.SqlClient;

namespace Calories.BE.Data
{
    public class DapperContext
    {
        private readonly string _connectionString;
        public DapperContext(IOptions<AppConfigData> options)
        {
            _connectionString = options.Value.DbConnectionString;
        }
        public IDbConnection CreateConnection()
            => new SqlConnection(_connectionString);
    }
}
