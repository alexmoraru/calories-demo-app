﻿using AutoMapper;

namespace Calories.BE.Domain.Mapper
{
    public static class AppMapperConfig
    {
        public static MapperConfiguration Configure()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMapperConfigProfile>();
            });
        }
    }
}
