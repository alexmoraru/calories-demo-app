﻿using AutoMapper;
using Calories.BE.Domain.ApiModels.In;
using Calories.BE.Domain.ApiModels.Out;
using Calories.BE.Domain.DbModels;

namespace Calories.BE.Domain.Mapper
{
    public class AppMapperConfigProfile : Profile
    {
        public AppMapperConfigProfile()
        {
            CreateMap<FoodEntry, FoodEntryListViewOut>()
                .ForMember(x => x.Meal, dest => dest.MapFrom(src => src.Meal.Name));
            CreateMap<FoodEntry, FoodEntryUserListViewOut>()
                .ForMember(x => x.Meal, dest => dest.MapFrom(src => src.Meal.Name))
                .ForMember(x => x.User, dest => dest.MapFrom(src => src.User.Name));
            CreateMap<Meal, MealListViewOut>();
            CreateMap<User, UserOut>();
            CreateMap<FoodEntryIn, FoodEntry>();
            CreateMap<UserFoodEntryIn, FoodEntry>();
        }
    }
}
