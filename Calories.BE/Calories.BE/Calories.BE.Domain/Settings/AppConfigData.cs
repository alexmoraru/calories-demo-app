﻿namespace Calories.BE.Domain.Settings
{
    public class AppConfigData
    {
        public string DbConnectionString { get; set; }
    }
}
