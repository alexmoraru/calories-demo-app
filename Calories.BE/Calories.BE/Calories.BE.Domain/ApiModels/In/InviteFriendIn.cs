﻿namespace Calories.BE.Domain.ApiModels.In
{
    public class InviteFriendIn
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}
