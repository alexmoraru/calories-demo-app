﻿using System;

namespace Calories.BE.Domain.ApiModels.In
{
    public class FoodEntryIn
    {
        public string Name { get; set; }

        public int Calories { get; set; }

        public long MealId { get; set; }

        public DateTime Created { get; set; }
    }
}
