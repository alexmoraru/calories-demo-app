﻿namespace Calories.BE.Domain.ApiModels.In
{
    public class UserFoodEntryIn : FoodEntryIn
    {
        public long UserId { get; set; }
    }
}
