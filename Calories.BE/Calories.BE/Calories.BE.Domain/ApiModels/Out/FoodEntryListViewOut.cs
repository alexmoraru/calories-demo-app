﻿using System;

namespace Calories.BE.Domain.ApiModels.Out
{
    public class FoodEntryListViewOut
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public int Calories { get; set; }

        public string Meal { get; set; }

        public DateTime Created { get; set; }
    }
}
