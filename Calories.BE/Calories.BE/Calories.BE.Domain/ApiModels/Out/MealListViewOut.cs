﻿namespace Calories.BE.Domain.ApiModels.Out
{
    public class MealListViewOut
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
