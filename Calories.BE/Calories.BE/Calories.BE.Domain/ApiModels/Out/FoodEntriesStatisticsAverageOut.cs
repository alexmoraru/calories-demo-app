﻿namespace Calories.BE.Domain.ApiModels.Out
{
    public class FoodEntriesStatisticsAverageOut
    {
        public long UserId { get; set; }

        public string UserName { get; set; }

        public float Calories { get; set; }
    }
}
