﻿namespace Calories.BE.Domain.ApiModels.Out
{
    public class FoodEntryUserListViewOut : FoodEntryListViewOut
    {
        public long MealId { get; set; }

        public long UserId { get; set; }

        public string User { get; set; }
    }
}
