﻿namespace Calories.BE.Domain.ApiModels.Out
{
    public class UserOut
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }
    }
}
