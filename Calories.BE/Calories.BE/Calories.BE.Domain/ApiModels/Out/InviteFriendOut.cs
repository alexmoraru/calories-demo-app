﻿namespace Calories.BE.Domain.ApiModels.Out
{
    public class InviteFriendOut
    {
        public string Token { get; set; }
    }
}
