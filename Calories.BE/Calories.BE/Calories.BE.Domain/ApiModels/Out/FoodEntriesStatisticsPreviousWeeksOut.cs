﻿namespace Calories.BE.Domain.ApiModels.Out
{
    public class FoodEntriesStatisticsPreviousWeeksOut
    {
        public int LastWeek { get; set; }

        public int PreviousToLastWeek { get; set; }
    }
}
