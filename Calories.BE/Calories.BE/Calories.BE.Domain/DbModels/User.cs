﻿using Calories.BE.Domain.Constants;
using Dapper.Contrib.Extensions;

namespace Calories.BE.Domain.DbModels
{
    [Table("Users")]
    public class User
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public UserRole Role { get; set; }

        public int CaloriesLimit { get; set; }
    }
}
