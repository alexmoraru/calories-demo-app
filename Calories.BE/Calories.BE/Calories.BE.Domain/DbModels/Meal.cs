﻿using Dapper.Contrib.Extensions;

namespace Calories.BE.Domain.DbModels
{
    [Table("Meals")]
    public class Meal
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public int MaxEntries { get; set; }
    }
}
