﻿using Dapper.Contrib.Extensions;
using System;

namespace Calories.BE.Domain.DbModels
{
    [Table("FoodEntries")]
    public class FoodEntry
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public int Calories { get; set; }

        public DateTime Created { get; set; }

        public long MealId { get; set; }

        public long UserId { get; set; }

        [Write(false)]
        public Meal Meal { get; set; }

        [Write(false)]
        public User User { get; set; }
    }
}
