﻿namespace Calories.BE.Domain.Constants
{
    public enum UserRole
    {
        User = 1,
        Admin = 2
    }
}
