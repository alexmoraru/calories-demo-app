﻿using System;

namespace Calories.BE.Domain.Exceptions
{
    public class AuthException : Exception
    {
        public AuthException(string message) : base(message) { }
    }
}
