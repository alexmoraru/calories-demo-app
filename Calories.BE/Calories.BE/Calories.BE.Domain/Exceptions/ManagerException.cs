﻿using System;

namespace Calories.BE.Domain.Exceptions
{
    public class ManagerException : Exception
    {
        public ManagerException(string message) : base(message) { }
    }
}
