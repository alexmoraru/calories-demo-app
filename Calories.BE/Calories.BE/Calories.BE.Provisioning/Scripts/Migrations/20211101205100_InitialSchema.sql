﻿CREATE TABLE Users
(
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(150) NOT NULL,
	Email nvarchar(100) NOT NULL,
	Password nvarchar(100) NOT NULL,
	Role smallint NOT NULL,
	CaloriesLimit int NOT NULL DEFAULT 2100
);

CREATE TABLE Meals
(
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(200) NOT NULL,
	MaxEntries smallint NOT NULL DEFAULT 1
);

CREATE TABLE FoodEntries
(
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(250) NOT NULL,
	Calories int NOT NULL,
	Created datetime NOT NULL,
	MealId int FOREIGN KEY REFERENCES Meals(Id),
	UserId int FOREIGN KEY REFERENCES Users(Id)
);