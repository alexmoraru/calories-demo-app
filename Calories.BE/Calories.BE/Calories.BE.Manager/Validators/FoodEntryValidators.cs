﻿using Calories.BE.Domain.ApiModels.In;
using FluentValidation;

namespace Calories.BE.Manager.Validators
{
    public class FoodEntryInValidator : AbstractValidator<FoodEntryIn>
    {
        public FoodEntryInValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .MaximumLength(250);
            RuleFor(x => x.Calories)
                .NotEmpty()
                .GreaterThan(0);
            RuleFor(x => x.MealId)
                .NotEmpty()
                .GreaterThan(0);
        }
    }

    public class UserFoodEntryInValidator : AbstractValidator<UserFoodEntryIn>
    {
        public UserFoodEntryInValidator()
        {
            RuleFor(x => x.UserId)
                .NotEmpty()
                .GreaterThan(0);
            Include(new FoodEntryInValidator());
        }
    }
}
