﻿using Calories.BE.Domain.ApiModels.In;
using FluentValidation;

namespace Calories.BE.Manager.Validators
{
    public class InviteFriendValidator : AbstractValidator<InviteFriendIn>
    {
        public InviteFriendValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .MaximumLength(150);
            RuleFor(x => x.Email)
                .NotEmpty()
                .MaximumLength(100)
                .EmailAddress();
        }
    }
}
