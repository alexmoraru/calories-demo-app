﻿using AutoMapper;
using Calories.BE.Data.Interfaces;
using Calories.BE.Domain.ApiModels.Out;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calories.BE.Manager.Services
{
    public class MealsService
    {
        private readonly IMealsRepository repository;
        private readonly IMapper mapper;

        public MealsService(IMealsRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public async Task<List<MealListViewOut>> Get()
        {
            var models = await repository.Get();

            return mapper.Map<List<MealListViewOut>>(models);
        }
    }
}
