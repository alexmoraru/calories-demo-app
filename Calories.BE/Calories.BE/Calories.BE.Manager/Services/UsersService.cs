﻿using AutoMapper;
using Calories.BE.Data.Interfaces;
using Calories.BE.Domain.ApiModels.In;
using Calories.BE.Domain.ApiModels.Out;
using Calories.BE.Domain.DbModels;
using Calories.BE.Domain.Exceptions;
using Calories.BE.Manager.Helpers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calories.BE.Manager.Services
{
    public class UsersService
    {
        private readonly IUsersRepository repository;
        private readonly IMapper mapper;
        public UsersService(IUsersRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public async Task<List<UserOut>> GetAll()
        {
            var users = await repository.Get(Domain.Constants.UserRole.User);

            return mapper.Map<List<UserOut>>(users);
        }

        public async Task<string> GetToken(string email)
        {
            var user = await repository.Get(email);

            if (user == null)
            {
                throw new EntityNotFoundException("Invalid login credentials");
            }

            return TokenHelper.GenerateToken(user);
        }

        public async Task<int> GetCaloriesLimit(long id)
        {
            var user = await repository.Get(id);

            return user.CaloriesLimit;
        }

        public async Task<InviteFriendOut> Invite(InviteFriendIn input)
        {
            var user = new User
            {
                Name = input.Name,
                Email = input.Email,
                Role = Domain.Constants.UserRole.User,
                Password = Guid.NewGuid().ToString(),
                CaloriesLimit = 2100
            };

            user.Id = await repository.Add(user);

            return new InviteFriendOut
            {
                Token = TokenHelper.GenerateToken(user)
            };
        }
    }
}
