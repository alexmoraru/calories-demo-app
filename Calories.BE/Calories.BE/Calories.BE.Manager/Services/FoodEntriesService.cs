﻿using AutoMapper;
using Calories.BE.Data.Interfaces;
using Calories.BE.Domain.ApiModels.In;
using Calories.BE.Domain.ApiModels.Out;
using Calories.BE.Domain.DbModels;
using Calories.BE.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calories.BE.Manager.Services
{
    public class FoodEntriesService
    {
        private readonly IFoodEntriesRepository repository;
        private readonly IMealsRepository mealsRepository;
        private readonly IUsersRepository usersRepository;

        private readonly IMapper mapper;

        public FoodEntriesService(IFoodEntriesRepository repository, IMealsRepository mealsRepository, IUsersRepository usersRepository, IMapper mapper)
        {
            this.repository = repository;
            this.mealsRepository = mealsRepository;
            this.usersRepository = usersRepository;
            this.mapper = mapper;
        }

        public async Task<List<FoodEntryListViewOut>> Get(long userId)
        {
            var models = await repository.Get(userId);

            return mapper.Map<List<FoodEntryListViewOut>>(models);
        }

        public async Task<List<FoodEntryUserListViewOut>> Get()
        {
            var models = await repository.Get();

            return mapper.Map<List<FoodEntryUserListViewOut>>(models);
        }

        public async Task<FoodEntriesStatisticsPreviousWeeksOut> GetStatisticsPreviousWeeks()
        {
            var lastWeek = repository.GetCount(DateTime.Today.AddDays(-6), DateTime.Today.AddDays(1));
            var previousToLastWeek = repository.GetCount(DateTime.Today.AddDays(-13), DateTime.Today.AddDays(-6));

            await Task.WhenAll(lastWeek, previousToLastWeek);

            return new FoodEntriesStatisticsPreviousWeeksOut
            {
                LastWeek = lastWeek.Result,
                PreviousToLastWeek = previousToLastWeek.Result
            };
        }

        public async Task<List<FoodEntriesStatisticsAverageOut>> GetStatisticsAverage()
        {
            return await repository.GetAverageStatistics(DateTime.Today.AddDays(-6));
        }

        public async Task<long> Add(long userId, FoodEntryIn input)
        {
            await Validate(userId, input);

            var model = mapper.Map<FoodEntry>(input);

            model.UserId = userId;

            return await repository.Add(model);
        }

        public Task<long> Add(UserFoodEntryIn input)
        {
            return Add(input.UserId, input);
        }

        public async Task Update(long id, UserFoodEntryIn input)
        {
            await Validate(input.UserId, input);

            var model = mapper.Map<FoodEntry>(input);

            model.Id = id;

            await repository.Update(model);
        }

        public async Task Delete(long id)
        {
            await repository.Delete(id);
        }

        private async Task Validate(long userId, FoodEntryIn input)
        {
            var meal = await mealsRepository.Get(input.MealId);

            if (meal == null)
            {
                throw new ManagerException("Invalid meal");
            }

            var foodEntriesCount = await repository.GetCount(userId, meal.Id, input.Created);

            if (foodEntriesCount >= meal.MaxEntries)
            {
                throw new ManagerException("Cannot add more entries for the selected meal.");
            }

            var user = await usersRepository.Get(userId);
            if (user == null)
            {
                throw new ManagerException("Invalid user");
            }
        }
    }
}
